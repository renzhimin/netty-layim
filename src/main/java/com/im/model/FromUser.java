package com.im.model;


import lombok.Data;

@Data
public class FromUser {

    private String id;

    private String username;

    private String avatar;

    private String mine;

    private String content;

}
