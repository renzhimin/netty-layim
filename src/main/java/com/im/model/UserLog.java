package com.im.model;


import lombok.Data;

@Data
public class UserLog {

    private String id;

    private String content;

    private String uid;

    private String from;

    private String from_group;

    private int type;

    private String remark;

    private String href;

    private Integer read;

    private String time;

    private String createTime;

    private User user;

}
