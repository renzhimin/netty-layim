package com.im.model;

import lombok.Data;

import java.util.List;

/**
 * 分组
 */
@Data
public class Group {

    private String id;

    private String groupname;

    private List<User> list;

}
