package com.im.model;


import lombok.Data;

@Data
public class SystemMsg {
    /**
     * layim.getMessage({
     system: true //系统消息
     ,id: 1111111 //聊天窗口ID
     ,type: "friend" //聊天窗口类型
     ,content: '对方已掉线'
     });
     */
    private boolean system = true;

    private String id;

    private String type = "friend";

    private String content;

    private long timestamp = System.currentTimeMillis();

    private String groupId;

    private User user;
}
