package com.im.model;


import lombok.Data;

@Data
public class SocketRequest {

    private FromUser mine;

    private ToUser to;
}
