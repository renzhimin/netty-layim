package com.im.model;

import lombok.Data;

@Data
public class SimpleMsg {
    /**
     * username: parse.data.username //消息来源用户名
     ,avatar: parse.data.avatar //消息来源用户头像
     ,id: parse.data.id //消息的来源ID（如果是私聊，则是用户id，如果是群聊，则是群组id）
     ,type: parse.data.type //聊天窗口来源类型，从发送消息传递的to里面获取
     ,content: "helloworld" //消息内容
     ,cid: parse.data.cid //消息id，可不传。除非你要对消息进行一些操作（如撤回）
     ,mine: false //是否我发送的消息，如果为true，则会显示在右方
     ,fromid: parse.data.fromid //消息的发送者id（比如群组中的某个消息发送者），可用于自动解决浏览器多窗口时的一些问题
     ,timestamp: parse.data.timestamp //服务端时间戳毫秒数。注意：如果你返回的是标准的 unix 时间戳，记得要 *1000
     */

    private String username;

    private String avatar;

    private String id;

    private String type;

    private String content;

    private String cid;

    private boolean mine = false;

    private String fromid;

    private long timestamp = System.currentTimeMillis();
}
