package com.im.model;


import lombok.Data;

@Data
public class ToUser {


    private String id;

    private String groupname;

    private String avatar;

    private String name;

    //friend  group 两种类型    friend  群聊  group单聊  register 注册
    private String type;

    private long historyTime;


    private String username;

    private String status;

    private String sign;


}
