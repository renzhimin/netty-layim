package com.im.model;


import lombok.Data;

@Data
public class Param {

    private String name;

    private Integer type;

    private Integer page;

    private String userid;

}
