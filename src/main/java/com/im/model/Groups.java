package com.im.model;


import lombok.Data;

/**
 * 群组
 */
@Data
public class Groups {

    private String id;

    private String groupname;

    private String avatar;

    private String uid;
}
