package com.im.model;


import lombok.Data;

@Data
public class User {


    private String id;

    private String username;

    private String status;

    private String sign;

    private String avatar;

    private Integer isValid;

}
