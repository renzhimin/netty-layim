package com.im.controller.user;


import com.im.config.BaseBean;
import com.im.constant.ReturnType;
import com.im.model.*;
import com.im.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Api(description = "用户操作接口")
@RestController
@RequestMapping(value = "/user/")
public class UserController {


    @Autowired
    UserService userService;


    @ApiOperation(value = "查询用户信息",notes = "查询用户信息")
    @GetMapping(value = "getUserInfo")
    public BaseBean getUserInfo(String userId){
        Map<String,Object> map= userService.getUserInfo(userId);
        return BaseBean.success(map);
    }

    @GetMapping(value = "getGroupsMembers")
    public BaseBean getGroupsMembers(String id){
        Map<String,List<User>> map = userService.getGroupsMembers(id);
        return BaseBean.success(map);
    }



    @PostMapping(value = "getUserOrGroupsByCondition" )
    public BaseBean getUserOrGroupsByCondition(Param param){

        Object obj = userService.getUserOrGroupsByCondition(param);

        return BaseBean.success(obj);
    }

    @PostMapping(value = "getGroup")
    public BaseBean getGroup(String userid){
        List<Group> list = userService.getGroup(userid);
        return BaseBean.success(list);
    }

    /**
     *
     * @param toId  添加的好友id
     * @param userId 自己的id
     * @param groupId 分组id
     * @param content   添加好友的内容
     * @return
     */
    @PostMapping(value = "addUser")
    public BaseBean addUser(String toId,String userId,String groupId,String content){
        userService.addUser(toId,userId,groupId,content);
        return BaseBean.success();

    }


    @GetMapping(value = "getUserLog")
    public BaseBean getUserLog(String userId){
        List<UserLog> userLogs = userService.getUserLog(userId);
        return BaseBean.success(userLogs);
    }


    @PostMapping(value = "registerUser")
    public BaseBean registerUser(String name){
        String id = userService.registerUser(name);
        return BaseBean.success(id);
    }


    @PostMapping(value = "agree")
    public BaseBean agree(String id,String group){
        userService.agree(id,group);
        return BaseBean.success();
    }


    @PostMapping(value = "createGroup")
    public BaseBean createGroup(String uid,String groupname){
        String group = userService.createGroup(uid, groupname);
        return BaseBean.success(group);
    }

    @PostMapping(value = "createGroups")
    public BaseBean createGroups(String uid,String groupname){
        Groups groups = userService.createGroups(uid, groupname);
        return BaseBean.success(groups);
    }

    @PostMapping(value = "addUserToGroup")
    public BaseBean addUserToGroup(String uid,String groupid){
        int num = userService.isAddToGroups(uid,groupid);
        if (num==1){
            return BaseBean.error("已经加入该群组！！！");
        }
        User user = userService.addUserToGroup(uid,groupid);
        return BaseBean.success(uid);
    }


    @PostMapping(value = "refuseFriend")
    public BaseBean refuseFriend(String logid){
        userService.refuseFriend(logid);
        return BaseBean.success();
    }
}
