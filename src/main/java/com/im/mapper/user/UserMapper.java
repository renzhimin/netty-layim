package com.im.mapper.user;

import com.im.model.*;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface UserMapper {

    @Select("select * from user where id = #{userId}")
    public User getUserInfo(String userId);

    List<Group> getGroup(String userId);


    @Select("SELECT\n" +
            "\tb.id id,\n" +
            "\tb.group_name groupname,\n" +
            "\tb.avatar \n" +
            "FROM\n" +
            "\t( SELECT * FROM user_relate_groups WHERE user_id = #{userId} ) a\n" +
            "\tLEFT JOIN groups b ON a.groups_id = b.id")
    List<Groups> getGroups(String userId);

    List<User> getGroupsMembers(String id);

    @Select("\n" +
            "select a.*,IFNULL(b.is_valid,0) as isValid\n" +
            "from (\n" +
            "select *  from `user` where username like concat('%',#{name},'%') ) a left join \n" +
            "(\n" +
            "select *,1 is_valid from user where id in (\n" +
            "\n" +
            "select user_id from group_relate_user where group_id in (\n" +
            "select group_id from user_relate_group where user_id = #{userid}))) b on a.id = b.id where a.id != #{userid}")
    List<User> getUserByParam(Param param);


    @Select("select c.*,d.username userName,d.id userId,IF(FIND_IN_SET(#{userid},c.userIdStr)>0,1,0) isJoin\n" +
            "            from ( \n" +
            "            select a.*,count(b.id) as userCount,GROUP_CONCAT(b.user_id) userIdStr from (\n" +
            "            select id groupId,group_name groupname,avatar,main from `groups` where group_name like concat('%',#{name},'%')\n" +
            "            ) a left join groups_relate_user b on a.groupId = b.groups_id group by a.groupId) c left join `user` d on c.main = d.id\n" +
            "\t\t\t\t\t\t")
    List<Map<String,Object>> getGroupsByParam(Param param);

    @Select("select b.id,b.group_name groupname from user_relate_group a left join `group` b on a.group_id = b.id where a.user_id = #{userid}")
    List<Group> getGroupsNotContainUser(String userid);

    void addUser(@org.apache.ibatis.annotations.Param("toId") String toId,
                 @org.apache.ibatis.annotations.Param("userId") String userId,
                 @org.apache.ibatis.annotations.Param("groupId") String groupId,
                 @org.apache.ibatis.annotations.Param("content") String content,
                 @org.apache.ibatis.annotations.Param("str") String str);

    List<UserLog> getUserLog(String userId);

    String getUserInfoByName(String name);


    @Select("select id from `group` where `default` = 1")
    List<String> getDefaultGroup();

    void insertUser(@org.apache.ibatis.annotations.Param("id") String id,
                    @org.apache.ibatis.annotations.Param("name") String name,
                    @org.apache.ibatis.annotations.Param("avatar") String avatar,
                    @org.apache.ibatis.annotations.Param("sign") String sign);

    @Insert("INSERT INTO `user_relate_group`(`user_id`, `group_id`) VALUES ( #{s}, #{s1})")
    void insertUserRelateGroup(@org.apache.ibatis.annotations.Param("s") String s,@org.apache.ibatis.annotations.Param("s1") String s1);

    UserLog getLogInfo(String id);

    @Insert("INSERT INTO `group_relate_user`( `group_id`, `user_id`) VALUES ( #{group}, #{from});")
    void groupRelateUser(@org.apache.ibatis.annotations.Param("group") String group,@org.apache.ibatis.annotations.Param("from") String from);

    @Update("update im_log set is_agree = 1 where id = #{id}")
    void updateLogAgree(String id);

    @Insert("INSERT INTO `im`.`group` ( `id`, `group_name`, `descript`, `user_id`, `default` )\n" +
            "VALUES\n" +
            "\t( #{groupId}, #{groupname}, '', #{id}, 1 )")
    void createDefaultGroup(@org.apache.ibatis.annotations.Param("groupId") String groupId,
                            @org.apache.ibatis.annotations.Param("groupname") String groupname,
                            @org.apache.ibatis.annotations.Param("id") String id);

    @Insert("INSERT INTO `im`.`group` ( `id`, `group_name`, `descript`, `user_id`, `default` )\n" +
            "VALUES\n" +
            "\t( #{id}, #{groupname}, '', #{uid}, 0 );")
    void createGroup(@org.apache.ibatis.annotations.Param("uid") String uid,
                     @org.apache.ibatis.annotations.Param("groupname") String groupname,
                     @org.apache.ibatis.annotations.Param("id") String id);

    @Insert("INSERT INTO `user_relate_group` ( `user_id`, `group_id` )\n" +
            "VALUES\n" +
            "\t( #{uid}, #{id} )")
    void userRelateGroup(@org.apache.ibatis.annotations.Param("uid") String uid,
                         @org.apache.ibatis.annotations.Param("id") String id);

    @Insert("INSERT INTO `groups` ( `id`,`group_name`, `avatar`, `main` )\n" +
            "VALUES\n" +
            "\t(#{id}, #{groupname}, #{avatar}, #{uid} )")
    void createGroups(Groups groups);

    @Insert("INSERT INTO `user_relate_groups` (  `user_id`, `groups_id` )\n" +
            "VALUES\n" +
            "\t(#{uid}, #{id} )")
    void relateGroups(Groups groups);

    @Insert("INSERT INTO `groups_relate_user` ( `groups_id`, `user_id` )\n" +
            "VALUES\n" +
            "\t( #{id},#{uid})")
    void groupsRelateUser(Groups groups);


    @Select("select count(1) from groups_relate_user where user_id =#{arg0} and groups_id =#{arg1} limit 1 ")
    int isAddToGroups(String uid, String groupid);

    @Update("update im_log set is_agree = 2 where id = #{logid}")
    void refuseThisLog(String logid);

    @Insert("INSERT INTO `im_log`( `content`, `uid`, `from`, `from_group`, `type`, `remark`, `href`, `read`, `time`, " +
            "`create_time`, `user_id`, `is_agree`) VALUES ( concat(#{user.username},'拒绝你的好友申请','!!!'), " +
            "#{from}, null, " +
            "null, 1, '', NULL, 1, NULL, " +
            "now(), null, 0);")
    void addRefuseLog(UserLog logInfo);
}
