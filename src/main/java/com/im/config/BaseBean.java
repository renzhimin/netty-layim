package com.im.config;

import com.im.constant.ReturnType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class BaseBean<T> {



    private int code;

    private String msg;

    private Object data;

    private BaseBean(Object data){
        this.code = 0;
        this.msg = "success";
        this.data = data;
    }
    public BaseBean(){
        this(ReturnType.success);
    }


    public BaseBean(ReturnType returnType){
        this.code = returnType.getCode();
        this.msg = returnType.getMsg();
        this.data = null;
    }
    public BaseBean(ReturnType returnType,String msg){
        this.code = returnType.getCode();
        this.msg = msg;
        this.data = null;
    }


    public BaseBean(ReturnType returnType,Object data){
        this.code = returnType.getCode();
        this.msg = returnType.getMsg();
        this.data = data;
    }

    public static <T>BaseBean<T> success(T data){
        return new BaseBean<>(data);
    }

    public static <T>BaseBean<T> success(){
        return new BaseBean<>();
    }

    public static <T>BaseBean<T> error(){
        return new BaseBean<>(ReturnType.error);
    }
    public static <T>BaseBean<T> error(String msg){
        return new BaseBean<>(ReturnType.error,msg);
    }
}
