package com.im.service.user;

import com.im.model.*;

import java.util.List;
import java.util.Map;

public interface UserService {


    Map<String,Object> getUserInfo(String userId);

    Map<String,List<User>> getGroupsMembers(String id);

    Object getUserOrGroupsByCondition(Param param);

    List<Group> getGroup(String userid);

    void addUser(String toId, String userId, String groupId, String content);

    List<UserLog> getUserLog(String userId);

    String registerUser(String name);

    void agree(String id, String group);

    String createGroup(String uid, String groupname);


    Groups createGroups(String uid, String groupname);

    User addUserToGroup(String uid, String groupid);

    int isAddToGroups(String uid, String groupid);

    void refuseFriend(String logid);
}
