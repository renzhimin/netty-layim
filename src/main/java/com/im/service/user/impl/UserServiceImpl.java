package com.im.service.user.impl;

import com.im.mapper.user.UserMapper;
import com.im.model.*;
import com.im.netty.group.ChannelGroup;
import com.im.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {


    Random random = new Random();
    List<String> list = Arrays.asList
            ("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596518071282&di=800d439b7e5340e4f333a1d18245e47c&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201901%2F09%2F20190109072726_aNNZd.thumb.700_0.jpeg"
            ,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596518071282&di=bacb55988282ed79a0992a104dae5bbb&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201510%2F18%2F20151018172940_5etXi.jpeg"
                    ,"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2141623099,2896788564&fm=26&gp=0.jpg",
                    "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596518071282&di=3b5efe65d7b24bd9b3e2098a60eab334&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201812%2F31%2F20181231010435_scjtm.png",
                    "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596518071282&di=09864d0f6f4b9d1fe1bed3d48530788a&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201707%2F21%2F20170721162706_FnWZT.thumb.700_0.jpeg",
                    "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596518071281&di=cbb782cb3ffa422a7356fd65f08ef9ae&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201507%2F22%2F20150722123545_N5h3L.jpeg"
            );

    List<String> famousRemark = Arrays.asList("自己不能胜任的事情，切莫轻易答应别人，一旦答应了别人，就必须实践自己的诺言",
                                "君子在下位则多谤，在上位则多誉；小人在下位则多誉，在上位则多谤",
                                "你若要喜爱你自己的价值，你就得给世界创造价值",
                                "君子赠人以言，庶人赠人以财",
                                "时间会刺破青春表面的彩饰，会在美人的额上掘深沟浅槽；会吃掉稀世之珍！天生丽质，什么都逃不过他那横扫的镰刀");

    @Autowired
    ChannelGroup channelGroup;

    @Autowired
    UserMapper userMapper;
    @Override
    public Map<String,Object> getUserInfo(String userId) {
        Map<String, Object> map = new HashMap<>();
        User userInfo = userMapper.getUserInfo(userId);
        map.put("mine",userInfo);
        List<Group> groups = userMapper.getGroup(userId);
        map.put("friend",groups);
        List<Groups> list = userMapper.getGroups(userId);
        map.put("group",list);
        return map;
    }

    @Override
    public Map<String, List<User>> getGroupsMembers(String id) {
        List<User> list = userMapper.getGroupsMembers(id);
        return coverDataToMap("list",list);
    }

    @Override
    public Object getUserOrGroupsByCondition(Param param) {

        Object obj = null;

        switch (param.getType()){
            case 0:
                //查询好友
                obj = userMapper.getUserByParam(param);
                break;
            case 1:
                obj = userMapper.getGroupsByParam(param);
                break;
            default:
                break;
        }

        return obj;
    }

    @Override
    public List<Group> getGroup(String userid) {
        //查询分组信息  不包含成员
        List<Group> list = userMapper.getGroupsNotContainUser(userid);
        return list;
    }

    @Override
    public void addUser(String toId, String userId, String groupId, String content) {
        userMapper.addUser(toId,userId,groupId,content,"申请添加你为好友");
        //发送消息给好友
        channelGroup.sendMsgForAddFriend(toId);

    }

    @Override
    public List<UserLog> getUserLog(String userId) {
        return userMapper.getUserLog(userId);
    }

    @Override
    @Transactional
    public String registerUser(String name) {
        String s = UUID.randomUUID().toString().replaceAll("-", "");
        String id = userMapper.getUserInfoByName(name);
        if (!StringUtils.isEmpty(id)){
            return id;
        }
        //查询默认分组
        //创建默认分组
        String groupId = UUID.randomUUID().toString().replaceAll("-", "");
        userMapper.createDefaultGroup(groupId,"默认分组",s);
        List<String> group = Arrays.asList(groupId);
        userMapper.insertUser(s,name,list.get(random.nextInt(list.size())),
                famousRemark.get(random.nextInt(famousRemark.size())));
        if (group !=null && group.size()>0){
            for (String s1 : group) {
                userMapper.insertUserRelateGroup(s,s1);
            }
        }

        return s;
    }

    @Override
    public void agree(String id, String group) {

        //查询该条添加记录
        UserLog userLog = userMapper.getLogInfo(id);
        userMapper.groupRelateUser(group,userLog.getFrom());
        //双向添加好友
        userMapper.groupRelateUser(userLog.getFrom_group(),userLog.getUid());
        userMapper.updateLogAgree(id);

        channelGroup.agree(userLog.getFrom(),userLog.getUid(),userLog.getFrom_group());


    }

    @Override
    public String createGroup(String uid, String groupname) {

        String id = UUID.randomUUID().toString().replaceAll("-", "");
        userMapper.createGroup(uid,groupname,id);
        userMapper.userRelateGroup(uid,id);
        return id;
    }
    List<String > lists = Arrays.asList("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596640550300&di=f742fd8f68776ce5c8bba465e50cbe11&imgtype=0&src=http%3A%2F%2Fd.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2Fe61190ef76c6a7efd517f640fbfaaf51f3de66a6.jpg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596640550300&di=e2f69aa97a015cdf8595fb20502be04f&imgtype=0&src=http%3A%2F%2Fattach.bbs.miui.com%2Fforum%2F201203%2F28%2F215200n0zayunz18ueaaa2.jpg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596640550299&di=cf4fd00dd91c478332c3e12a870fe377&imgtype=0&src=http%3A%2F%2Fcdn.duitang.com%2Fuploads%2Fitem%2F201508%2F08%2F20150808085253_ctAEQ.thumb.700_0.jpeg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596640550299&di=b03d23a29416e1694dffdffda7526807&imgtype=0&src=http%3A%2F%2Fimage.biaobaiju.com%2Fuploads%2F20180802%2F02%2F1533149026-hXezDjaMqd.jpeg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596640550299&di=c710a1fb42233ff6a1c8e457d81bab98&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fwallpaper%2F1208%2F15%2Fc5%2F12943863_1345046042782.jpg");

    @Override
    @Transactional
    public Groups createGroups(String uid, String groupname) {
        Groups groups = new Groups();
        groups.setAvatar(lists.get(random.nextInt(lists.size())));
        groups.setId(UUID.randomUUID().toString().replaceAll("-",""));
        groups.setGroupname(groupname);
        groups.setUid(uid);
        userMapper.createGroups(groups);
        //用户与分组关联
        userMapper.relateGroups(groups);
        //分组与用户关联
        userMapper.groupsRelateUser(groups);
        return groups;
    }

    @Override
    @Transactional
    public User addUserToGroup(String uid, String groupid) {
        User userInfo = userMapper.getUserInfo(uid);
        Groups groups = new Groups();
        groups.setUid(uid);
        groups.setId(groupid);
        userMapper.relateGroups(groups);
        userMapper.groupsRelateUser(groups);
        return userInfo;
    }

    @Override
    public int isAddToGroups(String uid, String groupid) {
        int num = userMapper.isAddToGroups(uid,groupid);
        return num;
    }

    @Override
    @Transactional
    public void refuseFriend(String logid) {
        UserLog logInfo = userMapper.getLogInfo(logid);
        userMapper.refuseThisLog(logid);
        userMapper.addRefuseLog(logInfo);
    }

    public Map coverDataToMap(String key,Object data){
        return new HashMap<String,Object>(){
            {
                put(key,data);
            }
        };
    }
}
