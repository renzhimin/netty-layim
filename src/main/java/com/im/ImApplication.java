package com.im;


import com.im.netty.server.NettyServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("com.im.mapper")
public class ImApplication {


    public static void main(String[] args) {


        SpringApplication.run(ImApplication.class,args);
    }
}
